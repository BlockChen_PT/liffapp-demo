# -*- coding=UTF-8 -*-

import hmac
import hashlib
import base64


def validate(secret, msgbody, requestSignature):
    dig = hmac.new(secret, msg=msgbody, digestmod=hashlib.sha256).digest()
    serverSignature = base64.b64encode(dig)
    print("Server sign: %s" % serverSignature)
    print("Request sign: %s" % requestSignature)

    if serverSignature==requestSignature:
        return True
    else:
        return False
        
if __name__=="__main__":
    CHANNEL_SECRET = '51b0ab12e9a426f470d294c9d11017a1'.encode('utf-8')
    
    body='{"events":[{"type":"message","replyToken":"490fa254eba7471d91799816e74e0221","source":{"userId":"U43e93c97270521b26d7698c58b1aac92","type":"user"},"timestamp":1578559307126,"mode":"active","message":{"type":"text","id":"11227049266776","text":"測試"}}],"destination":"U65854ea06dda1c51906b0a2d0f4d5ff1"}'
    requestSignature = "nxg4Tslm343gdTh6o0zNzthBf6fnHfC561D8q1J3gLk="
    
    ispass = validate(CHANNEL_SECRET, body, requestSignature)
    
    print("Validate: %s" % ispass)
# 依賴:
##
1.  PYTHON 3.5 以上
2.  FLASK 1.1.1 以上 (pip install flask)
3.  LINEBOT (pip install linebot)

# 前置準備:
## 
1. 在`https://developers.line.biz/`上申請channel
2. 在channel的messaging API設定Webhook URL (指向服務的callback連結)
3. 在channel的liff設定Endpoint URL(指向服務的liffapp連結)
4. 若使用本地端(localhost)提供服務,則還需要ngrok連結本地端,見參考資料2

# 參考資料:
##
1. https://tpu.thinkpower.com.tw/tpu/articleDetails/1528
2. https://tpu.thinkpower.com.tw/tpu/articleDetails/1567

# 運行:
##
1. 修改testserver.py裡面的`CHANNELID, ACCESS_TOKEN, CHANNEL_SECRET`
2. 執行python testserver.py
3. 打開瀏覽器, 網址輸入localhost:8080, 顯示OK表示服務已經運作

# 服務說明:
## localhost:8080 
1. GET方法: 回應200, 內文為"OK"
2. POST方法: 回應200, 內文為"OK"
## localhost:8080/callback 
1. GET方法: 回應200, 內文為"OK"
2. POST方法: 回應200, 內文為"OK",會額外調用`handle_message`方法推送訊息, 內文為`We got msg: %s from user id: %s`
## localhost:8080/liffapp
1. GET方法: 回應200, 內文為index.html
2. POST方法: 回應200, 內文為index.html

![avatar](example.jpg)

# 其他程式說明:
##
1. channel-signature-validate.py, 驗證channel與請求的簽名範例
2. pushmsg.py, 往line服務器主動推送訊息範例
3. test-http-request.py, 對`http://127.0.0.1:8080`執行GET請求的範例